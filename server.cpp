/*
 *  TicTacToe Server - Author: Robert McGillivray
 */
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string>
#include <cstring>
#include <sstream>
#include <iostream>

using namespace std;

char boardState[9] = {' ',' ',' ',
                      ' ',' ',' ',
                      ' ',' ',' '};//initial board state


/**
  * Preconditions: none
  * Postconditions: sets all values in the boardState array to ' '
  */
void resetBoard() { //a function to set all positions on board to empty (' ')
    for(int i = 0; i < sizeof(boardState); i++){
        boardState[i] = ' ';
    }
}

/**
  * Preconditions: none
  * Posttconditions: will return a formatted string with the board state
  */
string getBoard() { //a function for providing a grphical representation of the board state withing ASCII symbols and returning a string
    string boardString = "";//the blank starting string
    for(int i = 0; i < sizeof(boardState); i++){//for each space on the board
        boardString += boardState[i];//add the board value to the string
        if((i+1)%3 == 0 && i+1 != sizeof(boardState)){//if the board has listed 3 valuse and this isn't the last row,
            boardString += "\n-----\n";//add a row delimiter to the string
        }else if(i+1 != sizeof(boardState)){//if you are not on the 3rd element in a row,
            boardString += "|";//add a vertical delimiter after the value
        }
    }
    return boardString;//return the formatted string
}

/**
  * Preconditions: gameWinnerStatus - A char representing the winner of the game, X, Y, or D for draw
  *                ticTacToeSocket - an integer representing the file descriptor for the connecting socket
  * Posttconditions: sends end-of-game information to the client and sets up the server for a new game
  */
void endGame(char gameWinnerStatus, int ticTacToeSocket) {
    string message;
    stringstream ss;
    message = "Board State:\n"+getBoard()+"\nGame won by: ";//show the final board and who won
    string gameWinnerStatusString = "";
    ss << gameWinnerStatus;
    ss >> gameWinnerStatusString;//get the game winner status as a string
    if(gameWinnerStatusString=="D"){//if the game winner status returned 'D'
        gameWinnerStatusString = "Draw";//then the game was a draw
    }
    message+= gameWinnerStatusString + "\n;";//add the winner status string to the message
    printf(message.c_str());//print the message to the server console
    send(ticTacToeSocket , message.c_str() , message.length() , 0 );//send the message to the client
    resetBoard();//reset the board state for the next game
    printf("\n\nNew Game:");//print a few new lines and an indication of the new game starting
}

/**
  * Preconditions: a    - an integer array to be searched
  *                len  - the length of the provided array
  *                c    - the char to be looked for
  * Postconditions: will return true if the char is present in the provided array
  */
bool arrayContains(int* a, int len, char c){//a function for checking if an array contains a specificed char
    for(int i = 0; i < len; i++){//for each indes in the array
        if(a[i]==c){//if the value matches the provided char
            return 1;//return true
        }
    }
    return 0;//fir none were ever found, return false
}

/**
  * A wrapper function for array contains, specifying the board as the array
  * Preconditions: any provided char will be looked for in the board
  * Postconditions: will return true if the char is present in the boardState
  */
bool boardContains(char c){//check if the board contains the specified character 
    return arrayContains((int*)(boardState),sizeof(boardState), c);
}

/**
  * Preconditions: any character provided will be searched for
  * Postconditions: returns the number of the specified character present in the board
  */
int countMoves(char c){//count the number of moves on the board for the specified character
    int ans = 0;
    for(int i = 0; i < sizeof(boardState); i++){
        if(boardState[i]==c){
            ans++;
        }
    }
    return ans;
}

/**
  * Preconditions: none
  * Postconditions: returns 'D' if there is a draw, 'X' if X wins, and 'O' if O wins. If the game is not at a 'complete' state, returns ' '
  */
char gameWon(){//check if the game board contains a game ending state
    int winStates[8][3] = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{6,4,2}};//all sets of tuples in a tic-tac-toe game that result in three in a row
    int numTuples = 8;
    int Ocount = countMoves('O');//number of O moves on the board
    int Xcount = countMoves('X');//number of X moves on the board
    int Ospaces[Ocount] = {};//array of spaces with a O
    int Xspaces[Xcount] = {};//array of spaces with an X
    int Ocurr = 0;//current number of O spaces
    int Xcurr = 0;//current number of X spaces
    for( int i = 0; i < sizeof(boardState); i++){//for each number in the board
        if(boardState[i]=='O'){//if the number is a O
            Ospaces[Ocurr++] = i;//add the board index to the array of O spaces and add on the the O count
        }
        if(boardState[i]=='X'){//if the number is a X
            Xspaces[Xcurr++] = i;//add the board index to the array of X spaces and add on the the X count
        }
    }
    for( int i = 0; i < numTuples; i++){//for each set of tuples in winStates
        int Ocheck = 0;//begin with 0 O's matching the positions in the current tuple 
        int Xcheck = 0;//begin with 0 X's too
        for( int j = 0; j < 3; j++){//for each of the 3 in the tuple
            if(arrayContains(Ospaces, Ocount, winStates[i][j])){//if the Ospaces array contains the location from the winState,
                Ocheck++;//add one to the Ocheck
            }
            if(arrayContains(Xspaces, Xcount, winStates[i][j])){//if the Xspaces array contains the location from the winState,
                Xcheck++;//add one to the Xcheck
            }
        }
        if(Xcheck >= 3){//if there were three valid X's in the tuples postions, then there are three X's in a row
            return 'X';//return that X has won
        }
        if(Ocheck >= 3){//if there were three valid O's in the tuples postions, then there are three O's in a row
            return 'O';//return that O has won
        }
    }
    if(Xcount + Ocount>=sizeof(boardState)){//if the number of O and X spaces combined is 9 then the board is full and the result is a draw
        return 'D';//return draw result
    }
    return ' ';//if there are still valid moves left to be made, and neither X nor O has threee in a row, then report ' '
}

/**
  * Preconditions: accepts 0, 1, or 2 command line arguments. 
  *     If there is one, it is the port number. 
  *     If there are two command line arguments, the first is the server address and the second is the server port.
  * Posttconditions: will return -1 upon failure
  */
int main(int argc, char **argv){//the main function of the server. run when the program is launched
    srand (time(NULL));//seeding the C++ random number generator with the time
    int server_fd, ticTacToeSocket, valread; //set up server file descriptor, socket, and 
    struct sockaddr_in address;//setting up the socket struct
    int opt = 1;//option for socket creation
    int addrlen = sizeof(address); //length of the socket struct
    char buffer[1024] = {0};//buffer of 0's for loading input from client
    string sPort = "";//string representation of the port to look for connections on
    if (argc > 1){//if there were more than one arguments in the command line
        sPort = argv[1];//the first argument is the port
     } else {//if there were no command line arguments
        cout << "Please provide port number[8181]: ";//ask for the port number, indicating the default is 8181.
        sPort = cin.get();//get the user's input
	if(sPort == "" || sPort == "\n"){//if the user just hit enter,
            sPort = "8181";//set the port to 8181
        }
    }

    stringstream convInput(sPort);//convert the string representation of the port to a string stream
    int port;//make a new int to store the port number
    convInput >> port;//set the port to the number from the string

    printf("Setting up connection. Listening to port %d\n",port);//inform the user we have started listening

    //create the socket for the client to connect to and store the server file descriptor
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){ 
        printf("socket failed"); //report errors
        return -1; //and exit
    } 

    //set up the options for the socket
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,&opt, sizeof(opt))){ 
        printf("setsockopt failed"); 
        return -1;
    } 
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( port ); 
       
    //set the port number for the socket to the previously selected port
    if (bind(server_fd, (struct sockaddr *)&address,sizeof(address))<0){ 
        printf("bind failed"); 
        return -1; 
    }
    //begin listen for connections on this server
    if (listen(server_fd, 3) < 0) { 
        printf("listen failed"); 
        return -1; 
    }

    while(1){//while true loop to last forever and ever, untill the user kills the program.
        //accept new connections and save them in the ticTacToeSocket 
        if ((ticTacToeSocket = accept(server_fd, (struct sockaddr *)&address,(socklen_t*)&addrlen))<0){ 
            printf("accept failed"); 
            return -1; 
        }
        //after accepting a connection the program will continue on through the while loop.
        string message = "Welcome to Tic-Tac-Toe:\n"+getBoard()+"\nPlease enter a number 0-8 to make a move.\n";//give the user an introduction and some instructions
        send(ticTacToeSocket , message.c_str() , message.length() , 0 );//send the message to the user
        while(1){//this while loop runs for the entirety of one game, then breaks after the game finishes
            memset(buffer,0,1024);//clear the input buffer
            valread = read( ticTacToeSocket , buffer, 1024);//read the input value from the client into the buffer
            printf("Read move: %s\n",buffer );//report the read move to the server console
            int move = (int)buffer[0]-(int)'0';//get the integer location of the client's move
            while( move<0 || move>sizeof(boardState) || boardState[move]!=' '){//while the move is invalid (out of bounds, or occupied)
                string errorMessage = "Invalid move. Please enter a number 0-8. Be sure the space is empty!";//inform the user of their mistake
                printf("%s",errorMessage.c_str());//print the message for the client to the server's console
                send(ticTacToeSocket , errorMessage.c_str() , errorMessage.length() , 0 );//send the message to the client
                memset(buffer,0,1024);//clear the input buffer
                valread = read( ticTacToeSocket , buffer, 1024);//read the input value from the client 
                move = (int)buffer[0]-(int)'0';//get the integer location of the client's NEW move
            }//continue to loop and check the client input values until the provide a valid one
            boardState[(int)buffer[0]-(int)'0']='X';//set the selectd position to an X
            int resp = rand()%sizeof(boardState);//get a new random number 0-8
            char gameWinnerStatus = gameWon();//get a char that represents if the game has been won or is in a draw
            printf("Winner status: %c",gameWinnerStatus);
            if(gameWinnerStatus == ' '){//if the game has not been won and the board is not full
                while (boardState[resp]!=' '){//while the space selected is not empty
                    resp = rand()%sizeof(boardState);//pick a new random number 
                }//and continue the loop until you find an empty space
                boardState[resp] = 'O';//set the randomly chosen empty poition to a O
                printf("Response move sent at position %d\n",resp);//print to the server console where the move was made
            }else{//if the game was won or in a draw
                endGame(gameWinnerStatus,ticTacToeSocket);
                break;//break out of this current game loop
            }
            //this checks the game for a win condition after the computer has made a move
            gameWinnerStatus = gameWon();//get a char that represents if the game has been won or is in a draw
            if(gameWinnerStatus != ' '){//if the game has been won or the board is not full
                endGame(gameWinnerStatus,ticTacToeSocket);
                break;//break out of this current game loop
            }
            message = "Board State:\n"+getBoard()+"\n";//a message with the board state
            printf("Sending message: \n%s\n",message.c_str());//print the baord state to the server console
            send(ticTacToeSocket , message.c_str() , message.length() , 0 );//send the board state to the client
        }//ends game loop
    }//ends listening for more clients loop
    return 0;
} 
