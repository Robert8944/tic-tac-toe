/*
 *  TicTacToe Client - Author: Robert McGillivray
 */
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <sstream>
#include <iostream>

using namespace std;

/**
  * Preconditions: accepts 1 or 0 command line arguments. 
  *     If there is one, it is the port number. 
  * Posttconditions: will return -1 upon failure
  */
int main(int argc, char **argv) {//begin main function; loaded when the program is run
    struct sockaddr_in address; //setting up socket information and address name
    int sock = 0, valread; 
    struct sockaddr_in serv_addr; 
    char buffer[1024] = {0}; 
    string addr;//store the server address that will be used by default

    string sPort = "";//setting an empty port string for use
    if (argc > 2){//if there were more than 2 arguments in the command line (including the program call as an argument)
        addr = argv[1];//set the ip address to the first command line argument after the program name
        sPort = argv[2];//set the port to be used to the second command line argument after the program name
    } else if(argc > 1){//if there was more than 1 command line argument
        sPort = argv[1];//set the port to be used to the first command line argument after the program name
    }else {//if there was not an argument provided after the program name
        cout << "Please provide the server address[127.0.0.1]: ";//ask for an address, defaulting to localhost
        getline(cin, addr);//get an address from the user
        if(addr == "" || addr == "\n"){//if the user just hit enter
            addr= "127.0.0.1";//use the default address
        }
        cout << "Please provide a port number[8181]: ";//ask for a port number, defaulting to 8181
        getline(cin, sPort);//get a port number from the user
        if(sPort == "" || sPort == "\n"){//if the user just hit enter
            sPort = "8181";//use the default port
        }
    }

    //get the port as an integer.
    stringstream convInput(sPort);
    int port = 0;
    convInput >> port;

    cout << "Setting up connection on port " << port << "\n";//inform the user of the setup

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){//check that the socket was created correctly
        printf("Error in socket creation");//report and return error
        return -1; 
    }

    //setting up the server address
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port); 
       
    //Get server address added to serv_addr const 
    if(inet_pton(AF_INET, addr.c_str(), &serv_addr.sin_addr)<=0) { 
        printf("Error in address"); 
        return -1; 
    } 

    //connect to the socket
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { 
        printf("Failed to connect"); 
        return -1; 
    }
    string message = "";
    while(1){//a loop to ask for new input from the client
        memset(buffer,0,1024);//clear the memory buffer for reading client input
        valread = read( sock , buffer, 1024);//read the value into the buffer from the socket
        string serverResp(buffer,1024);//format the buffer response into a string
        if(serverResp.find(";")!=string::npos){//if the response has the termination char ";" in it
            printf("%s",buffer );//then print the message
            return 0;//and end the program
        }else{//if it was a normal message
            printf("%s",buffer );//print it out
        }
        getline(cin, message);//get more input from the user
	    while(message.length()>1){//while the message is a character or more
            printf("Please give single character responces. Try again:\n");//ask for a smaller input
            getline(cin, message);//get the new input
        }
        send(sock , message.c_str() , message.length() , 0 );//send the message from the user
    }
    return 0; 
} 
