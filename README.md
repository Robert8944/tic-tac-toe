# Tic-Tac-Toe - Auther: Robert McGillivray

Running Instructions:
These instructions assume you are using a Linux terminal with g++ avalible

1.) Extract the zip archive

2.) From inside the extracted archive, run "make"
	-this runs the included makefile with the default "all" option
	-it also uses g++ to compile the files

3.) Run the server program with "./server"
	-you can also run this with a command line argument for the port
	-the program will ask for a port if one wasn't provided
	-If you just press enter when prompted, the default port (8181) will be used

4.) Leave the server program running in the background!

5.) Run the client program with "./client"
	-you can also run this with a 1 or 2 extra command line arguments
	-if there is one command line argument it will be used as the port
	-if there are two the first will be used as the server address and the second as the port number

6.) The client will be prompted to make a move and be shown a representation of a tic-tac-toe board.

7.) Moves are made by inputting a single digit to the client from 0 to 8, with their positions shown below:

		0|1|2
		-----
		3|4|5
		-----
		6|7|8

8.) After one of the players completes a row of 3 or the board becomes completely full, the game ends. The result is reported to both the client and the server console

9.) The server will continue running and listening for a new connection on the same port number, and the client will close itself.
